import {observables} from 'rxjs';
import {component} from '@angular/core';
import {translateService} from '@ngx-translate/core';

@component ({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls:['./app.component.css']
})
export class appComponent {
	title='angular-whishlist';
	time= new observable( observer=> {
	setinterval(()=> observer.next(new Date().toString()),1000);
	return null;
	});

 constructor(private translate:TranslateService){
 console.log('   get translation');
 translate.getTranslation('en').subscribe(x=> console.log('x= ' +JSON.stringify(x)));
 translate.setDefaultlang('es');
 }

 destinoAgregado(d) {
 
 }
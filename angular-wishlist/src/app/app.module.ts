import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './destino-viaje/destino-viaje.component';
import { ListadestinosComponent } from './listadestinos/listadestinos.component';

@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListadestinosComponent,
    EspiameDirective,
    TrackearClickDirective
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
  

})

export class AppModule { }

import {storeDevtoolsModule} from '@angx/store-devtools';
import {ngxMapboxGLModule} from "@ngx-mapbox-gl";
import {AppComponent} from './app.component';
import {destinoviajecomponent} from './components/destino-viaje/destino-viaje-component';
import {listaDestinoscpmponent} from './components/lista-destino/lista-destinos-component';
import {destinodetallecomponent} from './components/destino-detalle/destino-detalle-component';
import {form DestinoViajecomponent} from './components/form-destino-viaje/form-destino-viaje-component';
import {browserAnimationsModule} from '@ngular/plataform-brwser/animations';


import {
  DestinosViajesState,
  intializeDestinosViajesState,
  reducerDestinosViajes,
  DestinosViajesEffect
}from './models/destino-viajes-state.model';
import {loginComponent} from './components/login/login/login-component';
import {PritectedComponent} from './components/protected/protected/protected-component';
import {UsuarioLogeadoGuard} from './guard/usuario-logeado/usuario-logueado-guard';
import {AuthService} from './services/auth.service';
import {VuelosComponentComponent} from './components/vuelos/vuelos-main-component/vuelos-component.component';
import {VuelosMainComponentComponent} from './components/vuelos/vuelos-component/vuelos-main-component.component';
import {VuelosMasInfoComponentComponent} from './components/vuelos/vuelos-mas-info-component/vuelos-mas-info-component.component';
import {VuelosDetalleComponent} from './components/vuelos/vuelos-detalle-component/vuelos-detalle-component.component';
import {reservables} from './reservas/reservas.module';
import  DExie from "dexie";
import { EspiameDirective } from './espiame.directive';
import { TrackearClickDirective } from './trackear-click.directive';

//appconfig
export interface appconfig {
  apiEndpoint:string;
}


export const APP_CONFIG = new InjectionToken<appconfig>('app.config')

export const childrenRoutesVuelos:Routes =[
{ path:'',redirectTo: 'main',pathMatch:'full'},
{ path:'main',component: VuelosMainComponentComponent},
{ path:'mas-info',component: VuelosMasInfoComponentComponent},
{ path:':id',component: VuelosDetalleComponent},

];

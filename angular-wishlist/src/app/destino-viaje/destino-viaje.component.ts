import { Component, OnInit,input host Binding} from '@angular/core';
import{DestinoViaje} from './../models/destino-viaje.model';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {
@input() destino: destinoViaje;
@hostBinding('attr.class') cssClass='col-md-4';
@output() clicked: eventEmitter<destinoviaje>;

  constructor() {
 this.clicked=new EventEmitter();
  }

  ngOnInit(): void {
  }

}
ir(){
this.clicked.emit(this.destino);
	return false;
}
import {
	reducerDestinosViajes,
	DestinosViajesState,
	initializeDestinosViajesState,
	InitMyDataAction,
	NuevoDestioAction,
} from './destinos-viajes-state.model';
 import {DestinoViaje} from '.destino-viaje.model';

 describe ('reducerDestinosViajes', () => {
   it('should reduce init data', () => {
     const prevstate: DestinosViajesState = intializedestinosViajesState();
     const action: initMyDataAction = new initMyDataAction(['destino 1', 'destino 2']);
     const newState: DestinoViajesState = reducerDestinosViajes(prevstate,action);
     expect(newState.item.lenght).toEqual(2);
     expect(newState.item[0].nombre).toEqual('destino 1');
   });

   it('should reduce new item added',() => {
      const prevstate: DestinosViajesState = intializedestinosViajesState();
      const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('barcelona','url'));
      const newState: DestinoViajesState = reducerDestinosViajes(prevstate,action);
      expect(newState.item.lenght).toEqual(1);
      expect(newState.item[0].nombre).toEqual('barcelona');
   });
 });



 